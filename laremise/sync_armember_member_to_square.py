import logging
import sys
from typing import Any, Dict

from laremise import armember_helper, square_helper
from laremise.armember_helper import ARMemberHelper
from laremise.square_helper import SquareHelper


def do_the_sync(
    ah: ARMemberHelper, sh: SquareHelper, armember_id: int
) -> Dict[str, Any]:
    logging.info(f"syncing ARMember to Square: armember-id={armember_id}")

    # Try with reference_id
    reference_id = f"armember-{armember_id}"
    customers = sh.search_customers(reference_id=reference_id)
    if len(customers) > 0:
        if len(customers) > 1:
            logging.warn(
                f"More than one Square customers have reference id {armember_id}, returning first"
            )

        customer = customers[0]
        logging.info(f"Found using reference id: square-customer-id={customer['id']}")
        return customer

    logging.debug("Not found using reference id")

    # Try with email
    am = ah.member_details(armember_id)
    if am.email:
        customers = sh.search_customers(email=am.email)

        if len(customers) > 0:
            if len(customers) > 1:
                logging.warn(
                    f"More than one Square customers have email {am.email}, returning first"
                )

            logging.info("Found using email")
            customer = customers[0]

            # Write ARMember member id as Square customer's reference field.
            sh.update_customer(
                customer["id"], customer["version"], reference_id=reference_id
            )

            logging.debug("Wrote ARMember id as Square's reference id")
            return customer

        logging.debug("Not found using email")
    else:
        logging.debug("ARMember member has no email")

    logging.info("Creating new Square customer profile")
    return sh.create_customer(
        am.first_name, am.last_name, am.email, reference_id=reference_id
    )


def main():
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    ah = armember_helper.get_from_env()
    sh = square_helper.get_from_env()
    do_the_sync(ah, sh, int(sys.argv[1]))


if __name__ == "__main__":
    main()
