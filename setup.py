import setuptools

setuptools.setup(
    name="laremise_helpers",
    version="0.0.1",
    author="Simon Marchi",
    author_email="simon.marchi@polymtl.ca",
    packages=[
        "square_helper",
        "myturn_helper",
        "armember_helper",
        "sync_armember_member_to_myturn",
        "sync_armember_member_to_myturn_app",
        "sync_myturn_member_to_square",
        "sync_myturn_member_to_square_app",
        "inventaire_atelier_velo",
        "inventaire_atelier_velo_app",
    ],
    package_data={
        "sync_armember_member_to_myturn_app": ["static/*", "templates/*", "app.wsgi"],
        "sync_myturn_member_to_square_app": ["static/*", "templates/*"],
        "inventaire_atelier_velo_app": ["templates/*"],
    },
    install_requires=[
        "squareup",
        "flask",
        "xmltodict",
        "beautifulsoup4",
    ],
)
