import calendar
import collections
import logging
import pickle
import re
import sys

import square_helper
from dateutil import parser

location_id_atelier_velo = "LMDX7V1Z4DYQ8"

logging.basicConfig(level=logging.WARN, stream=sys.stdout)

only_year = None
only_month = None

if len(sys.argv) > 1:
    only_year = int(sys.argv[1])

if len(sys.argv) > 2:
    only_month = int(sys.argv[2])

sh = square_helper.get_from_env()
payments = sh.list_payments(location_id=location_id_atelier_velo)

with open("dump", "wb") as f:
    pickle.dump(payments, f)


# with open("dump", "rb") as f:
#    payments = pickle.load(f)

# print(f'{len(payments)} payment objects')

payments = list(filter(lambda x: x["status"] == "COMPLETED", payments))

# print(f'{len(payments)} completed payment objects')

order_ids = list(map(lambda x: x["order_id"], payments))

# print(f'{len(order_ids)} order ids')

orders = sh.get_orders(order_ids)

# with open("orders", "rb") as f:
#    orders = pickle.load(f)

with open("orders", "wb") as f:
    pickle.dump(orders, f)

# Build an order id -> order map.
orders = {o["id"]: o for o in orders}

# pprint(orders)


def classify_item(item):
    if "name" not in item:
        # On va supposer.
        return "commande"

    name = item["name"]

    if re.search(r"\d\d-\d\d\d", name):
        return "commande"

    if name == "Commande catalogue":
        return "commande"

    if name in ("Part sociale", "Membre"):
        return "part-sociale"

    if "Abonnement annuel" in name:
        return "abonnement-annuel"

    if "Accès 1 jour" in name:
        return "acces-1-jour"

    if name in ("Vélo Complet", "Vélo complet"):
        return "velo-complet"

    if name in (
        "Câble",
        "Gaine",
        "Quincaillerie",
        "Patins de frein",
        "Chaine",
        "Rayon",
        "Roue",
        "Chambre à air",
        "Rustines à vulcanisation",
        "Porte gourde",
        "Maillon de chaine rapide",
        "Cage de billes pédalier",
        "Réflecteur",
        "Guidoline",
        "Cartouche de pédalier",
        "Cassette",
        "Axe de roue",
        "Manette vitesse",
        "Poignées",
        "Tube coude V-Brake",
        "Ruban de jante",
        "Potence",
        "Roue-libre",
        "Rustines autocollantes",
        "Leviers à pneus TL-4.2 (paire)",
        "Pneu",
        "Dérailleur arrière",
        "Pedale",
        "Détache-rapide (levier)",
        "Coude",
        "Tube V-Brake",
        "Dérailleur Arrière Shimano SIS",
    ):
        return "piece"

    if name == "Dons":
        return "dons"

    if name == "Entreposage Hiver":
        return "entreposage"

    if name == "Pièce usagée":
        return "piece-usagee"

    if name == "contributions volontaires événement vélo":
        return "dons-evenements"

    # pprint(item)
    raise RuntimeError("Don't know how to classify item")


# annee -> mois -> classe
d = {}

for payment in payments:
    # print("----")
    order = orders[payment["order_id"]]

    if "line_items" not in order:
        # We could make up a dummy item...
        print(f'No items in order {order["id"]}')
        continue

    created_at = payment["created_at"]
    date = parser.parse(created_at)
    # print(date)

    if only_year is not None and date.year != only_year:
        continue

    if only_month is not None and date.month != only_month:
        continue

    if date.year not in d:
        d[date.year] = {}

    year_bucket = d[date.year]

    if date.month not in year_bucket:
        year_bucket[date.month] = collections.Counter()

    counter = year_bucket[date.month]

    for item in order["line_items"]:
        name = item.get("name", "<unknown name>")

        # if "variation_name" in item:
        #    print(f"{name} (variation: {item['variation_name']})")
        # else:
        #    print(f"{name}")

        item_class = classify_item(item)

        # counter[item_class]
        # pprint(item)

        total_money = item["total_money"]
        assert total_money["currency"] == "CAD"
        amount = total_money["amount"]

        # Les taxes sont incluses sur tout sauf les parts sociales et les dons.
        if item_class not in ("part-sociale", "dons"):
            amount = int(amount / 1.05 / 1.095)

        counter[item_class] += amount

# pprint(d)

for year in sorted(d.keys()):
    year_bucket = d[year]

    for month in sorted(year_bucket.keys()):
        counter = year_bucket[month]

        _, last_day = calendar.monthrange(year, month)

        date = f"{year}-{month:02}-{last_day}"

        for cls in sorted(counter.keys()):
            # On omet les parts sociales, parce que ça ne va pas vraiment dans nos revenus.
            if cls == "part-sociale":
                continue

            amount = counter[cls]
            print(f"Square\t{date}\t{cls}\t\t{amount / 100.0:.2f}")

        print()
