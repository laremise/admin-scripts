import logging
import os
import re
import sys

from armember_helper import ARMemberHelper
from myturn_helper import MyTurnHelper

from laremise.sync_armember_member_to_myturn import sync_armember_member_to_myturn

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

ARMEMBER_WORDPRESS_BASE_URL = os.environ["ARMEMBER_WORDPRESS_BASE_URL"]
ARMEMBER_SECURITY_KEY = os.environ["ARMEMBER_SECURITY_KEY"]
MYTURN_USERNAME = os.environ["MYTURN_USERNAME"]
MYTURN_PASSWORD = os.environ["MYTURN_PASSWORD"]
SYNC_DIRECTORY = os.environ["ARMEMBER_TO_MYTURN_SYNC_DIRECTORY"]

ah = None
mh = None

with os.scandir(SYNC_DIRECTORY) as it:
    for entry in it:
        if not entry.is_file():
            continue

        if not re.match(r"^[0-9]+$", entry.name):
            continue

        if ah is None:
            ah = ARMemberHelper(ARMEMBER_WORDPRESS_BASE_URL, ARMEMBER_SECURITY_KEY)

        if mh is None:
            mh = MyTurnHelper(MYTURN_USERNAME, MYTURN_PASSWORD)

        try:
            myturn_member = sync_armember_member_to_myturn(ah, mh, int(entry.name))
            logging.info("Sync'ed member id {}: {}".format(entry.name, myturn_member))
            os.remove(entry.path)
        except Exception as e:
            logging.error("Failed to sync member id {}".format(entry.name))
            logging.error(e)
