import logging
import sys
import tempfile

from laremise import armember_helper, myturn_helper
from laremise.armember_helper import ARMemberHelper
from laremise.myturn_helper import MyTurnHelper


class MemberExistsException(Exception):
    def __init__(self, num: int, email: str):
        self._num = num
        self._email = email

    @property
    def num(self):
        return self._num

    @property
    def email(self):
        return self._email

    def __str__(self):
        return f"MemberExistsException(num={self.num}, email={self.email})"


class MissingEmailException(Exception):
    pass


def sync_armember_member_to_myturn(
    armember: ARMemberHelper, myturn: MyTurnHelper, id: int
):
    a_member = armember.member_details(id)

    if len(a_member.email) == 0:
        raise MissingEmailException()

    m_members = myturn.get_members(email=a_member.email)
    if len(m_members) > 0:
        raise MemberExistsException(len(m_members), a_member.email)

    return myturn.create_member(a_member.first_name, a_member.last_name, a_member.email)


def main():
    try:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
        ah = armember_helper.get_from_env()
        mh = myturn_helper.get_from_env()
        id = int(sys.argv[1])
        id = sync_armember_member_to_myturn(ah, mh, id)
        logging.info(f"Member créé, id = {id}")
    except myturn_helper.LoginException as e:
        print(e)
        with tempfile.NamedTemporaryFile(
            prefix="login-exception-", delete=False, mode="w"
        ) as f:
            f.write(e.content)
        raise


if __name__ == "__main__":
    main()
