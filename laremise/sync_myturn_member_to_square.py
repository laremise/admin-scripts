import logging
import sys
import tempfile

from laremise import myturn_helper, square_helper


class MemberNotFoundException(Exception):
    pass


class MultipleMembersFoundException(Exception):
    pass


class CustomerExistsException(Exception):
    def __init__(self, num: int):
        self._num = num

    @property
    def num(self):
        return self._num


class MissingEmailException(Exception):
    pass


def sync_myturn_member_to_square(
    myturn: myturn_helper.MyTurnHelper, square: square_helper.SquareHelper, id: int
):
    m_members = myturn.get_members(id=id)
    if len(m_members) == 0:
        raise MemberNotFoundException()

    if len(m_members) > 1:
        raise MultipleMembersFoundException()

    m_member = m_members[0]

    if not m_member.email:
        raise MissingEmailException()

    s_customers = square.search_customers(email=m_member.email)
    if len(s_customers) > 0:
        raise CustomerExistsException(len(s_customers))

    square.create_customer(m_member.first_name, m_member.last_name, m_member.email)


def main():
    try:
        logging.basicConfig(level=logging.INFO, stream=sys.stdout)
        mh = myturn_helper.get_from_env()
        sh = square_helper.get_from_env()
        id = int(sys.argv[1])
        sync_myturn_member_to_square(mh, sh, id)
    except myturn_helper.LoginException as e:
        print(e)
        with tempfile.NamedTemporaryFile(
            prefix="login-exception-", delete=False, mode="w"
        ) as f:
            f.write(e.content)
        raise


if __name__ == "__main__":
    main()
