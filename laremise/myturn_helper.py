import collections
import logging
import os
import sys
import tempfile
from typing import Any, Dict, List, Optional

import requests
import xmltodict
from bs4 import BeautifulSoup, Tag


class LoginException(Exception):
    def __init__(self, status: int, content: str):
        super().__init__()
        self.status = status
        self.content = content

    def __str__(self):
        return f"LoginException(status={self.status})"


class _HTTPError(Exception):
    def __init__(self, status: int):
        self._status = status

    @property
    def status(self):
        return self._status

    def __str__(self):
        return f"_HTTPError(status={self.status})"


class _CreateMemberError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self):
        return self._message

    def __str__(self):
        return f"_CreateMemberError(status={self.message})"


class Member:
    _id: str
    _first_name: str
    _last_name: str
    _email: str

    @property
    def id(self):
        return self._id

    @property
    def first_name(self):
        return self._first_name

    @property
    def last_name(self):
        return self._last_name

    @property
    def email(self):
        return self._email

    @staticmethod
    def from_dict(d: Dict[Any, Any]):
        m = Member()

        m._id = d["MembreID"]
        m._first_name = d["FirstName"]
        m._last_name = d["LastName"]
        m._email = d["Email"]

        return m

    def __repr__(self):
        return "Membre({} - {} {} - {})".format(
            self.id, self.first_name, self.last_name, self.email
        )


class MyTurnHelper:
    def __init__(self, username: str, password: str):
        self._username = username
        self._password = password
        self._logger = logging.getLogger(__name__)
        self._req = requests.Session()

        self._logger.debug("GET auth page")

        # Populate session with cookies.
        self._req.get(
            "https://montreal.myturn.com/library/myTurnLogin/auth",
            headers=self._make_headers(),
        )

        self._logger.debug("POST auth page")
        res = self._req.post(
            "https://montreal.myturn.com/library/j_spring_security_check",
            headers=self._make_headers(),
            data={"j_username": self._username, "j_password": self._password},
        )
        if res.status_code != 200:
            raise LoginException(res.status_code, res.text)

    @staticmethod
    def _make_headers(headers: Optional[Dict[str, str]] = None):
        if headers is None:
            headers = {}

        headers[
            "User-Agent"
        ] = "Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"
        headers['Accept-Language'] = 'en-US,en;q=0.5'
        return headers

    @staticmethod
    def _fixup_xml(xml: str):
        remplacement = {
            "First Name": "FirstName",
            "Last Name": "LastName",
            "Membre ID": "MembreID",
            "Member ID": "MembreID",
            "State/Province": "StateProvince",
            "Postal Code": "PostalCode",
            "Alt. Phone": "AltPhone",
            "Address Notes": "AddressNotes",
            "Secondary First Name": "SecondaryFirstName",
            "Secondary Last Name": "SecondaryLastName",
            "Secondary Email": "SecondaryEmail",
            "Secondary Title": "SecondaryTitle",
            "Secondary Organization": "SecondaryOrganization",
            "Secondary Address": "SecondaryAddress",
            "Secondary Address2": "SecondaryAddress2",
            "Secondary City": "SecondaryCity",
            "Secondary State/Province": "SecondaryStateProvince",
            "Secondary Postal Code": "SecondaryPostalCode",
            "Secondary Country": "SecondaryCountry",
            "Secondary Phone": "SecondaryPhone",
            "Secondary Alt. Phone": "SecondaryAltPhone",
            "Secondary Address Notes": "SecondaryAddressNotes",
            "Membre créé le (YYYY-MM-DD)": "DateCreation",
            "Start of first full membership (YYYY-MM-DD)": "DatePremierAbonnement",
            "Current Membership Type": "AbonnementActuel",
            "Latest Membership Change (request, upgrade, renewal, cancellation...) (YYYY-MM-DD)": "DateAbonnementDernierChangement",
            "Current Membership Expiration (YYYY-MM-DD)": "DateAbonnementActuelExpiration",
            "User Note": "UserNote",
            "User Warning": "UserWarning",
            "Household Type": "HouseholdType",
            "Household income range": "HouseholdIncomeRange",
            "Number of disabled people in household": "NumberOfDisabledPeopleInHousehold",
            "Renter/Homeowner": "RenterHomeowner",
            "Household Size": "HouseholdSize",
            "Opening Balance Date (YYYY-MM-DD)": "OpeningBalanceDate",
            "Opening Balance": "OpeningBalance",
            "Confirmed?": "Confirmed",
            "Unconfirmed Email": "UnconfirmedEmail",
            "Renews Automatically": "RenewsAutomatically",
            "Automatically Pay Statements": "AutomaticallyPayStatements",
            "Household income range Valeur": "HouseholdincomerangeValeur",
            "Renter/Homeowner Valeur": "RenterHomeownerValeur",
        }

        for avant, apres in remplacement.items():
            xml = xml.replace("<" + avant + ">", "<" + apres + ">")
            xml = xml.replace("</" + avant + ">", "</" + apres + ">")
            xml = xml.replace("<" + avant + " />", "<" + apres + " />")

        return xml

    def get_members(
        self, id: Optional[int] = None, email: Optional[str] = None
    ) -> List[Member]:
        self._logger.debug(f"get_members(id={id}, email={email})")

        if id is None and email is None:
            raise ValueError("At least one filter must be specified.")

        url = "https://montreal.myturn.com/library/orgMembership/exportUsers"
        params = {
            "format": "xml",
            "extension": "xml",
            "exportField": [
                "membership.attributes.membershipId",
                "firstName",
                "lastName",
                "emailAddress",
            ],
        }

        if id is not None:
            params["membershipId"] = str(id)

        if email is not None:
            params["emailAddress"] = email

        res = self._req.get(url, params=params, headers=self._make_headers())
        res.encoding = "utf-8"
        xml = res.text
        xml = xml.strip()

        if len(xml) == 0:
            return []

        self._logger.debug(xml)
        xml = self._fixup_xml(xml)
        self._logger.debug(xml)
        d = xmltodict.parse(xml)

        members: list[dict[Any, Any]] = []

        if type(d["nulls"]["user"]) in (collections.OrderedDict, dict):
            members.append(d["nulls"]["user"])
        elif type(d["nulls"]["user"]) == list:
            for candidate in d["nulls"]["user"]:
                members.append(candidate)
        else:
            logging.error(
                "unexpected type for d['nulls']['user']:", type(d["nulls"]["user"])
            )

        def do_filter(m: Dict[Any, Any]):
            if id is not None and m["MembreID"] != str(id):
                return False

            if email is not None and m["Email"] != email:
                return False

            return True

        return [Member.from_dict(m) for m in members if do_filter(m)]

    def create_member(self, first_name: str, last_name: str, email: str):
        self._logger.debug(
            f"create_member, first_name={first_name}, last_name={last_name}, email={email}"
        )

        form_url = "https://montreal.myturn.com/library/orgMembership/createUser"
        form_page_req = self._req.get(form_url, headers=self._make_headers())
        if form_page_req.status_code != 200:
            raise _HTTPError(form_page_req.status_code)

        form_page = form_page_req.text
        soup = BeautifulSoup(form_page, "html.parser")

        def get_one_input_value(name: str):
            input_tag = soup.find("input", attrs={"name": name})
            if type(input_tag) != Tag:
                raise RuntimeError(f"{name} input is not a Tag")

            value = input_tag["value"]
            if type(value) != str:
                raise RuntimeError(f"{name} value is not a string")

            return value

        sync_token = get_one_input_value("SYNCHRONIZER_TOKEN")
        sync_uri = get_one_input_value("SYNCHRONIZER_URI")
        next_url = get_one_input_value("nextUrl")

        data = {
            "SYNCHRONIZER_TOKEN": sync_token,
            "SYNCHRONIZER_URI": sync_uri,
            "nextUrl": next_url,
            "generateAuthData": "on",
            "_disableCartEmail": "",
            "firstName": first_name,
            "lastName": last_name,
            "emailAddress": email,
            "title": "",
            "organizationName": "",
            "address.street1": "",
            "address.street2": "",
            "address.city": "",
            "address.country": "CAN",
            "address.principalSubdivision": "",
            "address.postalCode": "",
            "address.phone": "",
            "address.phone2": "",
            "address.notes": "",
            "url": "",
            "sex": "",
            "_dateOfBirth": "",
            "dateOfBirth_date": "",
            "dateOfBirth": "struct",
            "dateOfBirth_tz": "America/Montreal",
            "dateOfBirth_time": "00:00",
            "firstName2": "",
            "lastName2": "",
            "emailAddress2": "",
            "title2": "",
            "organizationName2": "",
            "address2.street1": "",
            "address2.street2": "",
            "address2.city": "",
            "address2.country": "CAN",
            "address2.principalSubdivision": "",
            "address2.postalCode": "",
            "address2.phone": "",
            "address2.phone2": "",
            "address2.notes": "",
            "_dynamicFields.household_type": "y",
            "_dynamicFields.ethnicity": "y",
            "dynamicFields.disabled": "",
            "dynamicFields.household_size": "",
            "membership.memberSince_date": "",
            "membership.memberSince": "struct",
            "membership.memberSince_tz": "America/Montreal",
            "membership.memberSince_time": "00:00",
            "membershipId": "",
            "membershipTransition.newType": "3322",
            "membershipTransition.expiration_date": "",
            "membershipTransition.expiration": "struct",
            "membershipTransition.expiration_tz": "America/Montreal",
            "membershipTransition.expiration_time": "23:59",
            "amount": "10.00",
            "membershipTransition.amount": "10.00",
            "payments[0].method.id": "8",
            "payments[0].amount": "10.00",
            "payments[0].details.comment": "",
        }

        post_url = "https://montreal.myturn.com/library/orgMembership/saveNewUser"
        create_user_req = self._req.post(
            post_url, data=data, headers=self._make_headers()
        )

        if create_user_req.status_code != 200:
            raise _HTTPError(create_user_req.status_code)

        soup = BeautifulSoup(create_user_req.text, "html.parser")

        label = None
        for cur_label in soup.select("label"):
            if cur_label.text == "Membership ID":
                label = cur_label
                break

        if label is None:
            raise _CreateMemberError("Could not find id label.")

        parent = label.parent
        if not parent:
            raise RuntimeError("Could not find parent of label.")

        divs = parent.select(".form-control-static")

        if len(divs) != 1:
            raise _CreateMemberError(
                f"Unexpected number of div found (expected 1, got {len(divs)})."
            )

        return int(divs[0].text)


def get_from_env():
    return MyTurnHelper(os.environ["MYTURN_USERNAME"], os.environ["MYTURN_PASSWORD"])


def main():
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    try:
        myturn = get_from_env()
        if sys.argv[1] == "get-members-id":
            members = myturn.get_members(id=int(sys.argv[2]))
            print(members)
        if sys.argv[1] == "get-members-email":
            members = myturn.get_members(email=sys.argv[2])
            print(members)
        elif sys.argv[1] == "create-member":
            member_id = myturn.create_member(sys.argv[2], sys.argv[3], sys.argv[4])
            print(f"Created member (id={member_id})")
    except LoginException as e:
        print(f"Caught LoginException status={e.status}", file=sys.stderr)
        with tempfile.NamedTemporaryFile(
            prefix="login-exception-", delete=False, mode="w"
        ) as f:
            f.write(e.content)
        raise


if __name__ == "__main__":
    main()
