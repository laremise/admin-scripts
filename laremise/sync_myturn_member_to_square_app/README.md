Installation
------------

1. Créer un environnement virtual Python (`virtualenv`) et installer les
   paquets de cet entrepôt.
2. Copier `app.wsgi.example` vers `app.wsgi` et entrer les informations MyTurn
   et Square.
3. Configurer un serveur web pouvant servir une application WSGI.  Voici un
   exemple de configuration Apache / `mod_wsgi`:

    <VirtualHost *:443>
        ServerName remise.simark.ca

        WSGIDaemonProcess remise user=remise group=remise threads=1 python-home=/home/remise/env
        WSGIScriptAlias /myturn-to-square /home/remise/admin-scripts/sync_myturn_member_to_square_app/app.wsgi

        <Directory /home/remise/admin-scripts/sync_myturn_member_to_square_app>
            WSGIProcessGroup remise
            WSGIApplicationGroup %{GLOBAL}
            Require all granted
        </Directory>

        ErrorLog /home/remise/error.log
        CustomLog /home/remise/access.log combined

        # Bouts ajoutés par Let's Encrypt.
        SSLCertificateFile /etc/letsencrypt/live/remise.simark.ca/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/remise.simark.ca/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf
    </VirtualHost>
