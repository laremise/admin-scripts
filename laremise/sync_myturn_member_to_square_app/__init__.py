import tempfile

from flask import Flask, render_template, request

from laremise import myturn_helper, square_helper
from laremise.sync_myturn_member_to_square import (
    CustomerExistsException,
    MemberNotFoundException,
    MissingEmailException,
    MultipleMembersFoundException,
    sync_myturn_member_to_square,
)

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/sync")
def sync():
    if "id" not in request.args:
        return {
            "success": False,
            "reason": "Numéro de membre manquant.",
        }

    try:
        id = int(request.args["id"])
    except ValueError:
        return {
            "success": False,
            "reason": "Numéro de membre invalide.",
        }

    try:
        sh = square_helper.get_from_env()
        mh = myturn_helper.get_from_env()
        sync_myturn_member_to_square(mh, sh, id)
    except MemberNotFoundException:
        return {
            "success": False,
            "reason": "Aucun membre trouvé avec ce numéro.",
        }
    except MultipleMembersFoundException:
        return {
            "success": False,
            "reason": "Plusieurs membres trouvés avec ce numéro.",
        }
    except CustomerExistsException as e:
        return {
            "success": False,
            "reason": f"Il existe déjà {e.num} profil(s) client Square avec cette adresse courriel.",
        }
    except MissingEmailException:
        return {
            "success": False,
            "reason": "Le courriel est manquant dans le profil MyTurn.",
        }
    except myturn_helper.LoginException as e:
        with tempfile.NamedTemporaryFile(
            prefix=f"login-exception-{e.status}", delete=False, mode="w"
        ) as f:
            f.write(e.content)

        return {"success": False, "reason": "Erreur de connexion à MyTurn."}

    return {
        "success": True,
    }
