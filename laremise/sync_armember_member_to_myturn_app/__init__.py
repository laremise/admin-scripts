import os
import tempfile

from flask import Flask, render_template, request

from laremise import armember_helper, myturn_helper
from laremise.sync_armember_member_to_myturn import (
    MemberExistsException,
    MissingEmailException,
    sync_armember_member_to_myturn,
)

ARMEMBER_WORDPRESS_BASE_URL = os.environ["ARMEMBER_WORDPRESS_BASE_URL"]
ARMEMBER_SECURITY_KEY = os.environ["ARMEMBER_SECURITY_KEY"]
MYTURN_USERNAME = os.environ["MYTURN_USERNAME"]
MYTURN_PASSWORD = os.environ["MYTURN_PASSWORD"]
SYNC_DIRECTORY = os.environ["ARMEMBER_TO_MYTURN_SYNC_DIRECTORY"]

app = Flask(__name__, template_folder=os.path.dirname(__file__) + "/templates")


@app.route("/")
def index():
    return render_template("index.html")


def convert(id: int):
    ah = armember_helper.ARMemberHelper(
        ARMEMBER_WORDPRESS_BASE_URL, ARMEMBER_SECURITY_KEY
    )
    mh = myturn_helper.MyTurnHelper(MYTURN_USERNAME, MYTURN_PASSWORD)
    return sync_armember_member_to_myturn(ah, mh, id)


@app.route("/sync")
def sync():
    app.logger.info(f"sync called {request.args}")
    if "id" not in request.args:
        return {
            "success": False,
            "reason": "Numéro de membre manquant.",
        }

    try:
        id = int(request.args["id"])
    except ValueError:
        return {
            "success": False,
            "reason": "Numéro de membre invalide.",
        }

    sync = True
    if "async" in request.args:
        async_val = request.args.get("async")
        if async_val not in ("true", "false"):
            return {"success": False, "reason": "Bad `async` value"}
        if async_val == "true":
            sync = False

    if sync:
        try:
            id = convert(id)
            return {
                "success": True,
                "id": id,
            }
        except armember_helper.APIError as e:
            app.logger.warn(e)
            return {
                "success": False,
                "reason": "Erreur lors de l'obtention du membre ARMember",
            }
        except MemberExistsException as e:
            app.logger.warn(e)
            return {
                "success": False,
                "reason": f"Il existe déjà {e.num} compte(s) MyTurn avec cette adresse courriel.",
            }
        except MissingEmailException as e:
            app.logger.warn(e)
            return {
                "success": False,
                "reason": "Le courriel est manquant dans le profil ARMember.",
            }
        except myturn_helper.LoginException as e:
            app.logger.warn(e)
            with tempfile.NamedTemporaryFile(
                prefix=f"login-exception-{e.status}", delete=False, mode="w"
            ) as f:
                f.write(e.content)

            return {"success": False, "reason": "Erreur de connexion à MyTurn."}
        except Exception as e:
            app.logger.warn(e)
            return {"success": False, "reason": "Erreur inconnue."}
    else:
        try:
            p = os.path.join(SYNC_DIRECTORY, str(id))
            app.logger.info("Creating file {}".format(p))
            with open(p, "w"):
                pass
            return {
                "success": True,
            }
        except Exception as e:
            app.logger.warn(e)
            return {"success": False, "reason": "Erreur"}
