import logging
import os
import re
import sys
import threading

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from laremise.armember_helper import ARMemberHelper, NoSuchMemberException
from laremise.square_helper import SquareHelper
from laremise.sync_armember_member_to_square import do_the_sync

logging.basicConfig(level=logging.INFO, stream=sys.stdout)


SYNC_DIRECTORY = os.environ["ARMEMBER_TO_SQUARE_SYNC_DIRECTORY"]
ARMEMBER_WORDPRESS_BASE_URL = os.environ["ARMEMBER_WORDPRESS_BASE_URL"]
ARMEMBER_SECURITY_KEY = os.environ["ARMEMBER_SECURITY_KEY"]
SQUARE_ACCESS_TOKEN = os.environ["SQUARE_ACCESS_TOKEN"]

sync_lock = threading.Lock()


def do_sync():
    with sync_lock:
        ah = None
        sh = None

        print("do sync")
        print(threading.get_ident())

        with os.scandir(SYNC_DIRECTORY) as it:
            for entry in it:
                if not entry.is_file():
                    continue

                if not re.match(r"^[0-9]+$", entry.name):
                    continue

                if ah is None:
                    ah = ARMemberHelper(
                        ARMEMBER_WORDPRESS_BASE_URL, ARMEMBER_SECURITY_KEY
                    )

                if sh is None:
                    sh = SquareHelper(SQUARE_ACCESS_TOKEN)

                try:
                    logging.info(f"Syncing member id {entry.name}")
                    square_customer = do_the_sync(ah, sh, int(entry.name))
                    logging.info(
                        f"Sync'ed member id {entry.name}: square-customer-id={square_customer['id']}"
                    )
                    os.remove(entry.path)
                except NoSuchMemberException as e:
                    logging.error(e)

                    # Delete the file in this case, it likely won't work even if retrying.
                    os.remove(entry.path)
                except Exception as e:
                    logging.error(f"Failed to sync member id {entry.name}")
                    logging.error("aye", str(e))


class MyEventHandler(FileSystemEventHandler):
    def on_created(self, event):
        do_sync()


os.makedirs(SYNC_DIRECTORY)

# Do an initial sync
do_sync()

# Start watching the sync directory
observer = Observer()
observer.schedule(MyEventHandler(), SYNC_DIRECTORY)
observer.start()

try:
    # Check the sync directory every minute, in case we dropped some event,
    # for some reason.
    while observer.is_alive():
        observer.join(60)
        do_sync()
except KeyboardInterrupt:
    print("Got ^C, exiting.")
finally:
    observer.stop()
    observer.join()
