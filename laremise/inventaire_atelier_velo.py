import decimal
import logging
import sys
from typing import List

import square_helper
from square_helper import SquareHelper


class Variation:
    def __init__(
        self,
        item_name: str,
        var_name: str,
        price: decimal.Decimal,
        quantity: decimal.Decimal,
        sku: str,
    ):
        self._item_name = item_name
        self._var_name = var_name
        self._price = price
        self._quantity = quantity
        self._sku = sku

    @property
    def item_name(self):
        return self._item_name

    @property
    def var_name(self):
        return self._var_name

    @property
    def price(self) -> decimal.Decimal:
        return self._price

    @property
    def quantity(self) -> decimal.Decimal:
        return self._quantity

    @property
    def sku(self):
        return self._sku


# That will probably not change too often.
atelier_velo_location_id = "LMDX7V1Z4DYQ8"


def get_inventaire(square: SquareHelper) -> List[Variation]:
    counts = square.get_inventory_counts(location_ids=[atelier_velo_location_id])
    for count in counts:
        assert count["catalog_object_type"] == "ITEM_VARIATION"

    variation_ids = [count["catalog_object_id"] for count in counts]
    variations = square.get_catalog_objects(variation_ids)
    variations_dict = {var["id"]: var for var in variations}
    item_ids = list(set([var["item_variation_data"]["item_id"] for var in variations]))
    items = square.get_catalog_objects(item_ids)
    items_dict = {item["id"]: item for item in items}
    result: list[Variation] = []
    for c in counts:
        var_id = c["catalog_object_id"]
        if var_id not in variations_dict:
            continue

        var = variations_dict[c["catalog_object_id"]]
        var_data = var["item_variation_data"]
        item_id = var_data["item_id"]
        item = items_dict[item_id]
        item_data = item["item_data"]

        result.append(
            Variation(
                item_data["name"],
                var_data["name"],
                decimal.Decimal(var_data["price_money"]["amount"]) // 100,
                decimal.Decimal(c["quantity"]),
                var_data.get("sku", ""),
            )
        )

    return result


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    sh = square_helper.get_from_env()
    inventaire = get_inventaire(sh)
    inventaire.sort(key=lambda x: (x.item_name, x.var_name))
    total = 0
    for var in inventaire:
        print(
            f" - {var.item_name} | {var.var_name} | {var.price/100:.2f}$ | {var.quantity}"
        )
        total_item = var.quantity * var.price
        total += total_item

    print(f"{total/100:.2f}")
