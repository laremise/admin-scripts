import os

from flask import Flask, render_template, request

from laremise import armember_helper
from laremise.square_helper import SquareHelper
from laremise.sync_armember_member_to_square import do_the_sync

SYNC_DIRECTORY = os.environ["ARMEMBER_TO_SQUARE_SYNC_DIRECTORY"]
ARMEMBER_WORDPRESS_BASE_URL = os.environ["ARMEMBER_WORDPRESS_BASE_URL"]
ARMEMBER_SECURITY_KEY = os.environ["ARMEMBER_SECURITY_KEY"]
SQUARE_ACCESS_TOKEN = os.environ["SQUARE_ACCESS_TOKEN"]


app = Flask(__name__, template_folder=os.path.dirname(__file__) + "/templates")


@app.route("/")
def index():
    return render_template("index.html")


def convert(armember_id: int):
    ah = armember_helper.ARMemberHelper(
        ARMEMBER_WORDPRESS_BASE_URL, ARMEMBER_SECURITY_KEY
    )
    sh = SquareHelper(SQUARE_ACCESS_TOKEN)
    return do_the_sync(ah, sh, armember_id)


@app.route("/sync")
def sync():
    app.logger.info(f"sync called {request.args}")

    armember_id = request.args.get("id", type=int)
    if armember_id is None:
        return {
            "success": False,
            "reason": "Numéro de membre manquant ou invalide.",
        }

    sync = True
    if "async" in request.args:
        async_val = request.args.get("async")
        if async_val not in ("true", "false"):
            return {"success": False, "reason": "Bad `async` value"}
        if async_val == "true":
            sync = False

    if sync:
        try:
            convert(armember_id)
            return {
                "success": True,
            }
        except armember_helper.APIError as e:
            app.logger.warn(e)
            return {
                "success": False,
                "reason": "Erreur lors de l'obtention du membre ARMember",
            }
    else:
        try:
            p = os.path.join(SYNC_DIRECTORY, str(armember_id))
            app.logger.info("Creating file {}".format(p))
            with open(p, "w"):
                pass
            return {
                "success": True,
            }
        except Exception as e:
            app.logger.warn(e)
            return {"success": False, "reason": "Erreur"}
