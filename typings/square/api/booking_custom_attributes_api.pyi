"""
This type stub file was generated by pyright.
"""

from square.api.base_api import BaseApi

class BookingCustomAttributesApi(BaseApi):
    """A Controller to access Endpoints in the square API."""

    def __init__(self, config) -> None: ...
    def list_booking_custom_attribute_definitions(self, limit=..., cursor=...):
        """Does a GET request to /v2/bookings/custom-attribute-definitions.

        Get all bookings custom attribute definitions.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_READ` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_READ` and `APPOINTMENTS_READ` for the OAuth scope.

        Args:
            limit (int, optional): The maximum number of results to return in
                a single paged response. This limit is advisory. The response
                might contain more or fewer results. The minimum value is 1
                and the maximum value is 100. The default value is 20. For
                more information, see
                [Pagination](https://developer.squareup.com/docs/build-basics/c
                ommon-api-patterns/pagination).
            cursor (string, optional): The cursor returned in the paged
                response from the previous call to this endpoint. Provide this
                cursor to retrieve the next page of results for your original
                request. For more information, see
                [Pagination](https://developer.squareup.com/docs/build-basics/c
                ommon-api-patterns/pagination).

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def create_booking_custom_attribute_definition(self, body):
        """Does a POST request to /v2/bookings/custom-attribute-definitions.

        Creates a bookings custom attribute definition.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            body (CreateBookingCustomAttributeDefinitionRequest): An object
                containing the fields to POST for the request.  See the
                corresponding object definition for field details.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def delete_booking_custom_attribute_definition(self, key):
        """Does a DELETE request to /v2/bookings/custom-attribute-definitions/{key}.

        Deletes a bookings custom attribute definition.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            key (string): The key of the custom attribute definition to
                delete.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def retrieve_booking_custom_attribute_definition(self, key, version=...):
        """Does a GET request to /v2/bookings/custom-attribute-definitions/{key}.

        Retrieves a bookings custom attribute definition.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_READ` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_READ` and `APPOINTMENTS_READ` for the OAuth scope.

        Args:
            key (string): The key of the custom attribute definition to
                retrieve. If the requesting application is not the definition
                owner, you must use the qualified key.
            version (int, optional): The current version of the custom
                attribute definition, which is used for strongly consistent
                reads to guarantee that you receive the most up-to-date data.
                When included in the request, Square returns the specified
                version or a higher version if one exists. If the specified
                version is higher than the current version, Square returns a
                `BAD_REQUEST` error.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def update_booking_custom_attribute_definition(self, key, body):
        """Does a PUT request to /v2/bookings/custom-attribute-definitions/{key}.

        Updates a bookings custom attribute definition.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            key (string): The key of the custom attribute definition to
                update.
            body (UpdateBookingCustomAttributeDefinitionRequest): An object
                containing the fields to POST for the request.  See the
                corresponding object definition for field details.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def bulk_delete_booking_custom_attributes(self, body):
        """Does a POST request to /v2/bookings/custom-attributes/bulk-delete.

        Bulk deletes bookings custom attributes.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            body (BulkDeleteBookingCustomAttributesRequest): An object
                containing the fields to POST for the request.  See the
                corresponding object definition for field details.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def bulk_upsert_booking_custom_attributes(self, body):
        """Does a POST request to /v2/bookings/custom-attributes/bulk-upsert.

        Bulk upserts bookings custom attributes.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            body (BulkUpsertBookingCustomAttributesRequest): An object
                containing the fields to POST for the request.  See the
                corresponding object definition for field details.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def list_booking_custom_attributes(
        self, booking_id, limit=..., cursor=..., with_definitions=...
    ):
        """Does a GET request to /v2/bookings/{booking_id}/custom-attributes.

        Lists a booking's custom attributes.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_READ` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_READ` and `APPOINTMENTS_READ` for the OAuth scope.

        Args:
            booking_id (string): The ID of the target [booking]($m/Booking).
            limit (int, optional): The maximum number of results to return in
                a single paged response. This limit is advisory. The response
                might contain more or fewer results. The minimum value is 1
                and the maximum value is 100. The default value is 20. For
                more information, see
                [Pagination](https://developer.squareup.com/docs/build-basics/c
                ommon-api-patterns/pagination).
            cursor (string, optional): The cursor returned in the paged
                response from the previous call to this endpoint. Provide this
                cursor to retrieve the next page of results for your original
                request. For more information, see
                [Pagination](https://developer.squareup.com/docs/build-basics/c
                ommon-api-patterns/pagination).
            with_definitions (bool, optional): Indicates whether to return the
                [custom attribute definition]($m/CustomAttributeDefinition) in
                the `definition` field of each custom attribute. Set this
                parameter to `true` to get the name and description of each
                custom attribute, information about the data type, or other
                definition details. The default value is `false`.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def delete_booking_custom_attribute(self, booking_id, key):
        """Does a DELETE request to /v2/bookings/{booking_id}/custom-attributes/{key}.

        Deletes a bookings custom attribute.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            booking_id (string): The ID of the target [booking]($m/Booking).
            key (string): The key of the custom attribute to delete. This key
                must match the `key` of a custom attribute definition in the
                Square seller account. If the requesting application is not
                the definition owner, you must use the qualified key.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def retrieve_booking_custom_attribute(
        self, booking_id, key, with_definition=..., version=...
    ):
        """Does a GET request to /v2/bookings/{booking_id}/custom-attributes/{key}.

        Retrieves a bookings custom attribute.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_READ` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_READ` and `APPOINTMENTS_READ` for the OAuth scope.

        Args:
            booking_id (string): The ID of the target [booking]($m/Booking).
            key (string): The key of the custom attribute to retrieve. This
                key must match the `key` of a custom attribute definition in
                the Square seller account. If the requesting application is
                not the definition owner, you must use the qualified key.
            with_definition (bool, optional): Indicates whether to return the
                [custom attribute definition]($m/CustomAttributeDefinition) in
                the `definition` field of the custom attribute. Set this
                parameter to `true` to get the name and description of the
                custom attribute, information about the data type, or other
                definition details. The default value is `false`.
            version (int, optional): The current version of the custom
                attribute, which is used for strongly consistent reads to
                guarantee that you receive the most up-to-date data. When
                included in the request, Square returns the specified version
                or a higher version if one exists. If the specified version is
                higher than the current version, Square returns a
                `BAD_REQUEST` error.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
    def upsert_booking_custom_attribute(self, booking_id, key, body):
        """Does a PUT request to /v2/bookings/{booking_id}/custom-attributes/{key}.

        Upserts a bookings custom attribute.
        To call this endpoint with buyer-level permissions, set
        `APPOINTMENTS_WRITE` for the OAuth scope.
        To call this endpoint with seller-level permissions, set
        `APPOINTMENTS_ALL_WRITE` and `APPOINTMENTS_WRITE` for the OAuth
        scope.
        For calls to this endpoint with seller-level permissions to succeed,
        the seller must have subscribed to *Appointments Plus*
        or *Appointments Premium*.

        Args:
            booking_id (string): The ID of the target [booking]($m/Booking).
            key (string): The key of the custom attribute to create or update.
                This key must match the `key` of a custom attribute definition
                in the Square seller account. If the requesting application is
                not the definition owner, you must use the qualified key.
            body (UpsertBookingCustomAttributeRequest): An object containing
                the fields to POST for the request.  See the corresponding
                object definition for field details.

        Returns:
            ApiResponse: An object with the response value as well as other
                useful information such as status codes and headers. Success

        Raises:
            APIException: When an error occurs while fetching the data from
                the remote API. This exception includes the HTTP Response
                code, an error message, and the HTTP body that was received in
                the request.

        """
        ...
